# Projet Blog

L'objectif du projet est de créer une petite application de blog sans authentification en utilisant Spring Boot pour le backend et Angular pour le frontend.

 Fonctionnalités réalisées
- Création, consultation, modificationdes et suppression des articles



 Fonctionalités bonus 
. Existence d'une arre de recherche pour les articles, possibilité de faire des tests coté back et front et de laisser des commentaires.

Travail réalisé
.  Création de wireframes des différentes pages de l'application (mobile first)
.  Création de la base de données et les composants d'accès aux données pour la table avec JDBC (repository)
.  [Création d'une API Rest avec Spring-Boot (le contrôleur Article)](https://gitlab.com/f.serigne/blog-spring-boot)
.  Création du front responsive de l'application avec Angular
.  Requête de l'API Rest depuis les services de Angular
[Accueil](p1.png)
[Ajouter un Article](p2.png)
[Chercher un Article](p3.png)

Lien vers Monblog angular:[Monblog angular](https://gitlab.com/f.serigne/blog-angular)
