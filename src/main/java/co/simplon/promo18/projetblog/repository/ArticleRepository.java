package co.simplon.promo18.projetblog.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.projetblog.entity.Article;

@Repository
public class ArticleRepository {
    @Autowired
    private DataSource dataSource;

    public List<Article> findAll() {
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Article article = new Article(
                        rs.getInt("id"),
                        rs.getString("titre"),
                        rs.getDate("date").toLocalDate(),
                        rs.getString("contenu"));

                list.add(article);
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

        return list;
    }

    public Article findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Article article = new Article(
                        rs.getInt("id"),
                        rs.getString("titre"),
                        rs.getDate("date").toLocalDate(),
                        rs.getString("contenu"));

                return article;
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

        return null;
    }

    public void save(Article article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO article (titre, date, contenu) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, article.getTitre());
            stmt.setDate(2, Date.valueOf(article.getDate()));
            stmt.setString(3, article.getContenu());

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                article.setId(rs.getInt(1));
            }

        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id=?");
            stmt.setInt(1, id);

            return (stmt.executeUpdate() == 1);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }

    public boolean update(Article article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE article SET titre = ?, date = ?, contenu = ? WHERE id = ?");

            stmt.setString(1, article.getTitre());
            stmt.setDate(2, Date.valueOf(article.getDate()));
            stmt.setString(3, article.getContenu());
            stmt.setInt(4, article.getId());

            return (stmt.executeUpdate() == 1);

        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

    }

    // Méthode appelée quand on reçoit un "?titre=" dans le all() du Controller
    public List<Article> searchByTitre(String titre) {
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    /* SELECT * FROM article WHERE titre LIKE '%toto%' */
                    "SELECT * FROM article WHERE titre LIKE ?");
            stmt.setString(1, "%" + titre + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Article article = new Article(
                        rs.getInt("id"),
                        rs.getString("titre"),
                        rs.getDate("date").toLocalDate(),
                        rs.getString("contenu"));

                list.add(article);
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

        return list;
    }

    

}
