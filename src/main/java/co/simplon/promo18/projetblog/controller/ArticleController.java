package co.simplon.promo18.projetblog.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.projetblog.entity.Article;
import co.simplon.promo18.projetblog.repository.ArticleRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/article") // Indique un préfixe qui s'appliquera à tous les Mapping du contrôleur actuel
@Validated
public class ArticleController {

    @Autowired
    private ArticleRepository repo;

    @GetMapping
    public List<Article> all(@RequestParam Optional<String> titre) {
        if (!titre.isPresent()) {
            return repo.findAll();
        } else {
            return repo.searchByTitre(titre.get());
        }
    }

    private Article showOne(int id) {
        Article article = repo.findById(id);
        if (article == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return article;
    }

    @GetMapping("/{id}") // ex: http://localhost:8080/api/article/1
    public Article one(@PathVariable @Min(1) int id) {
        return this.showOne(id);
    }

    /**
     * Manière alternative de faire les routes avec verbe et paramètres,
     * qui ne se base pas sur les standards RESTful mais est néanmoins utilisé
     * en entreprise. Pour celle ci comme pour le /add, pas la peine de faire
     * les deux, juste on choisit son style entre REST ou RPC
     */
    @GetMapping("/show")
    public Article show(@RequestParam @Min(1) int id) {
        return this.showOne(id);
    }

    private Article createPost(Article article) {
        if (article.getDate() == null) {
            article.setDate(LocalDate.now());
        }

        repo.save(article);

        return article;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED) // Permet de renvoyer un code plus précis dans le cas d'un ajout réussi
    public Article add(@Valid @RequestBody Article article) {
        return this.createPost(article);
    }

    /**
     * Manière alternative de faire les routes d'ajout, qui ne se base pas sur les
     * standards RESTful mais est néanmoins utilisé en entreprise
     */
    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public Article addWithParams(
            @RequestParam @NotBlank String titre,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PastOrPresent LocalDate date,
            @RequestParam String contenu) {
        Article o = new Article(titre, date, contenu);
        return this.createPost(o);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT) // Permet de renvoyer un code plus précis dans le cas d'un ajout réussi
    public void delete(@PathVariable @Min(1) int id) {
        if (!repo.delete(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public Article update(@Valid @RequestBody Article article, @PathVariable @Min(1) int id) {
        if (id != article.getId()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if (!repo.update(article)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findById(article.getId());
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Article patch(@RequestBody Article article, @PathVariable @Min(1) int id, String contenu) {
        Article basear = repo.findById(id);
        if (basear == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        if (article.getTitre() != null)
            basear.setTitre(article.getTitre());
        if (article.getDate() != null)
            basear.setDate(article.getDate());
        if (article.getContenu() != null)
            basear.setContenu(article.getContenu());
        if (!repo.update(basear))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return basear;
    }

}
