package co.simplon.promo18.projetblog.entity;


import java.time.LocalDate;


public class Article {
    private Integer id;
    private String titre;
    private LocalDate date;
    private String contenu;
    public Article() {
    }
    public Article(String titre, LocalDate date, String contenu) {
        this.titre = titre;
        this.date = date;
        this.contenu = contenu;
    }
    public Article(Integer id, String titre, LocalDate date, String contenu) {
        this.id = id;
        this.titre = titre;
        this.date = date;
        this.contenu = contenu;
    }
    public Article(int int1, String string, LocalDate localDate, double double1) {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitre() {
        return titre;
    }
    public void setTitre(String titre) {
        this.titre = titre;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public String getContenu() {
        return contenu;
    }
    public void setContenu(String contenu) {
        this.contenu = contenu;
    }
    
}